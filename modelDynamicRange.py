import numpy as np
import matplotlib.pyplot as plt
from scipy.signal import stft
from sklearn.model_selection import train_test_split, cross_val_score, KFold
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score
import os
import pdb

# Function to find the key fob press segment

def find_keyfob_press_segment(data, threshold=0.05, segment_length=2048, window_size=100):
    """
    Find the segment of the signal where the key fob is pressed based on a threshold.
    """
    magnitude = np.abs(data)
    windowed_averages = np.convolve(magnitude, np.ones(window_size)/window_size, mode='same')
    press_indices = np.where(windowed_averages > threshold)[0]
    
    #print(f"Press Indices: {press_indices}")  # Debug print statement
    
    if len(press_indices) == 0:
        start_idx = 0
        end_idx = len(data)
    else:
        start_idx = press_indices[0]
        end_idx = press_indices[-1] + 1  # Include the last index
        
    segment = data[start_idx:end_idx]

    # Adjust the segment length if necessary
    if len(segment) < segment_length:
        # Pad the segment with zeros if it's shorter than the desired length
        segment = np.pad(segment, (0, segment_length - len(segment)), 'constant')
    else:
        # Truncate the segment if it's longer than the desired length
        segment = segment[:segment_length]
    
    return segment



# Function to read data, pad/truncate, and compute STFT
def compute_stft(segment, fs=1e6, nperseg=1024):
    f, t, Zxx = stft(segment, fs, nperseg=nperseg)
    Zxx_magnitude = np.abs(Zxx)
    Zxx_magnitude = np.where(Zxx_magnitude == 0, 1e-10, Zxx_magnitude)
    Zxx_log_magnitude = 20 * np.log10(Zxx_magnitude)
    Zxx_log_magnitude = np.clip(Zxx_log_magnitude, -100, 0)
    positive_freqs = f >= 0
    f_positive = f[positive_freqs]
    Zxx_log_magnitude_positive = Zxx_log_magnitude[positive_freqs, :]
    return f_positive, t, Zxx_log_magnitude_positive

# Directory containing the data files
data_dir = '/home/akouhpainejad/Desktop/tleCodes/myproject/gnuRadio/data/'

# List of files and their labels
files_labels = [
    ('kiaData1', 'kia'),
    ('kiaData2', 'kia'),
    ('kiaData3', 'kia'),
    ('kiaData4', 'kia'),
    ('kiaData5', 'kia'),
    
    ('vwData1', 'volksWagen'),
    ('vwData2', 'volksWagen'),
    ('vwData3', 'volksWagen'),
    ('vwData4', 'volksWagen'),
    ('vwData5', 'volksWagen'),

    ('dodgeTrain_part1', 'dodge'),
    ('dodgeTrain_part2', 'dodge'),
    ('dodgeTrain_part3', 'dodge'),
    ('dodgeTrain_part4', 'dodge'),
    ('dodgeTrain_part5', 'dodge')



]

# Prepare dataset
X = []
y = []

for file_name, label in files_labels:
    file_path = os.path.join(data_dir, file_name)
    data = np.fromfile(file_path, dtype=np.complex64)
    
    # Find the segment where the key fob is pressed
    segment = find_keyfob_press_segment(data, threshold=0.05)
    #print(f"Length of segment for {file_name}: {len(segment)}")  # Print segment length
    # Compute STFT on the segment
    f_positive, t, Zxx_log_magnitude_positive = compute_stft(segment)
    
    # Flatten the STFT magnitude for each file
    features = Zxx_log_magnitude_positive.flatten()
    X.append(features)
    y.append(label)

X = np.array(X)
y = np.array(y)

# Split the dataset into training and test sets
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=42)

# Train a Random Forest classifier
clf = RandomForestClassifier(n_estimators=100, random_state=42)
clf.fit(X_train, y_train)

# Evaluate the model
y_pred = clf.predict(X_test)
accuracy = accuracy_score(y_test, y_pred)
print(f'Accuracy: {accuracy * 100:.2f}%')


# Predict function for new data
def predict_key_fob(file_path, noise_std=0):
    data = np.fromfile(file_path, dtype=np.complex64)
    if noise_std > 0:
        data += np.random.normal(0, noise_std, data.shape)
    segment = find_keyfob_press_segment(data, threshold=0.05)
    f_positive, t, Zxx_log_magnitude_positive = compute_stft(segment)
    features = Zxx_log_magnitude_positive.flatten().reshape(1, -1)
    return clf.predict(features)[0]

# def predict_key_fob(file_path):
#     data = np.fromfile(file_path, dtype=np.complex64)
#     #data += np.random.normal(0.006833862304687501,data.size)
#     segment = find_keyfob_press_segment(data, threshold=0.05)
#     f_positive, t, Zxx_log_magnitude_positive = compute_stft(segment)
#     features = Zxx_log_magnitude_positive.flatten().reshape(1, -1)
#     return clf.predict(features)[0]

# Example prediction
new_file_path = '/home/akouhpainejad/Desktop/tleCodes/myproject/gnuRadio/data/dodgeTest'
predicted_label = predict_key_fob(new_file_path)
print(f'Predicted key fob: {predicted_label}')
