# KeyFobFreqAnalysis



## Information
- Model Predictor is a machine learning project based on tensor flow, the model uses random forest classifier to predict the model of pressed key fob based on the trained data of its radio frequency.

## Project status
- The project currently predicts the model of key fob from 3 different cars, this project has been trained using grabbed data from gnu rtl-sdr and training the model from using short time fourier transform.

## Data Collecting
- the data collector file can be run in gnu radio to write the data files for training the model.